//
//  ActionSheetPickerView.h
//  TextFieldTask
//
//  Created by Admin on 14.03.17.
//  Copyright © 2017 Nesynov. All rights reserved.
//

#import "LUNibBasedView.h"

@interface LUNibBasedView()

@property (nonatomic, assign, readwrite) BOOL isOpen;

@end

@implementation LUNibBasedView
@synthesize isOpen;

#pragma mark - Constants Public

CGFloat const LUPickerViewHeight = 250;

#pragma mark - Initialize

- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    [self xibSetup];
    
    return self;
}

- (id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    [self xibSetup];
    
    return self;
}

#pragma mark - Privat

- (void)initializeProperties {
}

- (UIView *)loadViewFromNib {
    NSBundle *bandle = [NSBundle bundleForClass:[LUNibBasedView class]];
    UINib *nib = [UINib nibWithNibName:NSStringFromClass([LUNibBasedView class])
                                bundle:bandle];
    UIView *view = [[nib instantiateWithOwner:self
                                      options:nil] firstObject];
    
    return view;
}

- (void)prepareForInterfaceBuilder {
    [self xibSetup];
}

- (void)xibSetup {
    UIView *view = [self loadViewFromNib];
    view.frame = self.bounds;
    view.autoresizingMask = UIViewAutoresizingFlexibleWidth |
    UIViewAutoresizingFlexibleHeight;
    [self addSubview:view];
    [self initializeProperties];
}

- (void)presentPicker {
    CGRect hostFrame = [[UIApplication sharedApplication].keyWindow frame];
    CGRect pickerContainerSourceFrame = CGRectMake(0,
                                                   hostFrame.size.height,
                                                   hostFrame.size.width,
                                                   LUPickerViewHeight);
    CGRect pickerTypeOneComponent = CGRectMake(0,
                                               hostFrame.size.height - LUPickerViewHeight,
                                               hostFrame.size.width,
                                               LUPickerViewHeight);
    self.frame = pickerContainerSourceFrame;
    [[UIApplication sharedApplication].keyWindow addSubview:self];
    [UIView animateWithDuration:0.2
                          delay:0
                        options:UIViewAnimationOptionCurveEaseOut
                     animations:^{
                         self.frame = pickerTypeOneComponent;
                     } completion:^(BOOL finished) {
                         self.isOpen = YES;
                     }];
}

- (void)dismissPicker {
    if (!self.isOpen) return;
    CGRect pickerTypeOneComponent = CGRectMake(0,
                                               self.frame.origin.y + LUPickerViewHeight,
                                               self.frame.size.width,
                                               LUPickerViewHeight);
    [UIView animateWithDuration:0.2
                          delay:0.f
                        options:UIViewAnimationOptionCurveEaseIn
                     animations:^{
                         self.frame = pickerTypeOneComponent;
                     } completion:^(BOOL finished) {
                         [self removeFromSuperview];
                         self.isOpen = NO;
                     }];
    
}

- (void)dismissPickerForce {
    if (!self.isOpen) return;
    [self removeFromSuperview];
    self.isOpen = NO;
}

#pragma mark - Actions

- (IBAction)okButtonTapped:(UIButton *)sender {
    self.selectCallBack(self.selected);
    [self dismissPicker];
}

- (IBAction)cancelButton:(UIButton *)sender {
    [self dismissPicker];
    self.selectCallBack(nil);
}

@end
