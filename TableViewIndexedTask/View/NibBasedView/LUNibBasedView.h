//
//  ActionSheetPickerView.h
//  TextFieldTask
//
//  Created by Admin on 14.03.17.
//  Copyright © 2017 Nesynov. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LUNibBasedView : UIView

@property (nonatomic, weak) IBOutlet UIView *pickerContainerView;

@property (nonatomic, strong) id selected;
@property (nonatomic, copy) void(^selectCallBack)(id);
@property (nonatomic, assign, readonly) BOOL isOpen;

- (void)presentPicker;
- (void)dismissPicker;
- (void)dismissPickerForce;

@end
