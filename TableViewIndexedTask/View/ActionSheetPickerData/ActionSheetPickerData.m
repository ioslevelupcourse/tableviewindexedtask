//
//  ActionSheetPickerData.m
//  TextFieldTask
//
//  Created by Admin on 14.03.17.
//  Copyright © 2017 Nesynov. All rights reserved.
//

#import "ActionSheetPickerData.h"

@interface ActionSheetPickerData ()

@property (nonatomic, weak) IBOutlet UIDatePicker *pickerData;

@property (nonatomic, strong) id startSelectItem;

@end

@implementation ActionSheetPickerData

#pragma mark - Initialize

+ (instancetype)pickerDataWithMode:(UIDatePickerMode)datePickerMode
                              from:(NSDate *)minimumDate
                                to:(NSDate *)maximumDate
                          interval:(NSInteger)minuteInterval
                   startSelectItem:(NSDate *)item
                    selectCallBack:(void(^)(id selected))selectCallBack {
    ActionSheetPickerData *actionSheetPickerData = [ActionSheetPickerData new];
    
    UIDatePicker *pickerData = [[UIDatePicker alloc] initWithFrame:actionSheetPickerData.pickerContainerView.bounds];
    pickerData.autoresizingMask = UIViewAutoresizingFlexibleWidth |
    UIViewAutoresizingFlexibleHeight;
    pickerData.datePickerMode = datePickerMode;
    pickerData.minimumDate = minimumDate;
    pickerData.maximumDate = maximumDate;
    pickerData.minuteInterval = minuteInterval;
    [pickerData addTarget:actionSheetPickerData
                   action:@selector(updateSelected:)
         forControlEvents:UIControlEventValueChanged];
    
    if (item) {
        pickerData.date = item;
        actionSheetPickerData.selected = item;
    } else {
        NSDate *currentDate = [NSDate date];
        actionSheetPickerData.selected = currentDate;
    }
    
    actionSheetPickerData.pickerData = pickerData;
    [actionSheetPickerData.pickerContainerView addSubview:pickerData];
    actionSheetPickerData.selectCallBack = selectCallBack;
    
    return actionSheetPickerData;
}

+ (instancetype)pickerDataWithStartSelectItem:(NSDate *)item
                               selectCallBack:(void(^)(id selected))selectCallBack {
    NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    NSDate *currentDate = [NSDate date];
    NSDateComponents *component = [[NSDateComponents alloc] init];
    [component setYear:-115];
    NSDate *minimumDate = [calendar dateByAddingComponents:component
                                                    toDate:currentDate
                                                   options:0];
    ActionSheetPickerData *actionSheetPickerData =
    [ActionSheetPickerData pickerDataWithMode:UIDatePickerModeDate
                                         from:minimumDate
                                           to:currentDate
                                     interval:0
                              startSelectItem:item
                               selectCallBack:selectCallBack];
    
    return actionSheetPickerData;
}

#pragma mark - Life Cycle

- (void)dealloc {
    [self.pickerData removeTarget:nil
                           action:NULL
                 forControlEvents:UIControlEventAllEvents];
}

#pragma mark - Action

- (void)updateSelected:(id)sender {
    self.selected = self.pickerData.date;
}

#pragma mark - Privat

- (void)createPikerDataInPickerContainerViewAndAddTarget {
    UIDatePicker *pickerData = [[UIDatePicker alloc] initWithFrame:self.pickerContainerView.bounds];
    pickerData.autoresizingMask = UIViewAutoresizingFlexibleWidth |
    UIViewAutoresizingFlexibleHeight;
    [self.pickerContainerView addSubview:pickerData];
    [pickerData addTarget:self
                   action:@selector(updateSelected:)
         forControlEvents:UIControlEventValueChanged];
    self.pickerData = pickerData;
}

@end
