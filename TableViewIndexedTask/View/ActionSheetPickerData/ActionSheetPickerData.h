//
//  ActionSheetPickerData.h
//  TextFieldTask
//
//  Created by Admin on 14.03.17.
//  Copyright © 2017 Nesynov. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "LUNibBasedView.h"

@interface ActionSheetPickerData : LUNibBasedView <UIPickerViewDelegate>

+ (instancetype)pickerDataWithMode:(UIDatePickerMode)datePickerMode
                              from:(NSDate *)minimumDate
                                to:(NSDate *)maximumDate
                          interval:(NSInteger)minuteInterval
                   startSelectItem:(NSDate *)item
                    selectCallBack:(void(^)(id selected))selectCallBack;

+ (instancetype)pickerDataWithStartSelectItem:(id)item
                               selectCallBack:(void(^)(id selected))selectCallBack;

@end
