
//
//  TableViewController.m
//  TableViewIndexedTask
//
//  Created by Admin on 21.04.17.
//  Copyright © 2017 Nesynov. All rights reserved.
//

#import "TableViewController.h"
#import "PersonViewController.h"

#import "PersonCell.h"
#import "ButtonCell.h"

#import "LUPerson.h"

#import "UITableView+LUExtentions.h"
#import "NSString+LUExtentions.h"
#import "NSArray+LUExtentions.h"
#import "NSMutableArray+LUExtentions.h"

#import "LUBlock.h"
#import "LUConstants.h"


@interface TableViewController () <UITableViewDataSource, UITabBarDelegate>

@property (nonatomic, strong) IBOutlet UITableView *tableView;

@property (nonatomic, copy)   NSDictionary  *indexPersons;
@property (nonatomic, copy)   NSArray       *personsKeysSorted;

@end

@implementation TableViewController

#pragma mark - Constants

const CGFloat kHeightForHeader = 10.f;
static NSString const * kLUBeyondSections = @"LUERROR: personsForSection going beyond sections";

#pragma mark - initialize

- (void)setIndexPersons:(NSMutableDictionary *)indexPersons {
    self.personsKeysSorted = [indexPersons.allKeys sortString];
    _indexPersons = indexPersons;
}

#pragma mark - Life Cycle

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self registerCell];
    self.indexPersons = [NSMutableDictionary new];
    
    for (LUPerson *person in [self createStartPerson]) {
        [self addPerson:person];
    }
}

#pragma mark - Privat

- (void)registerCell {
    UITableView *tableView = self.tableView;
    [tableView registerCellWithClass:[PersonCell class]];
    [tableView registerCellWithClass:[ButtonCell class]];
}

- (NSMutableArray *)createStartPerson {
    NSMutableArray *persons = [NSMutableArray new];
        [persons addObjectsFromArray:[self personsCount:1 withKey:@"2"]];
        [persons addObjectsFromArray:[self personsCount:1 withKey:@"4"]];
        [persons addObjectsFromArray:[self personsCount:2 withKey:@"6"]];
        [persons addObjectsFromArray:[self personsCount:2 withKey:@"D"]];
        [persons addObjectsFromArray:[self personsCount:2 withKey:@"E"]];
    
    return persons;
}

#pragma mark Data Persons

- (void)addPerson:(LUPerson *)person {
    NSIndexPath *indexPath = [self indexPathWithPerson:person];
    NSMutableDictionary *dic = [self.indexPersons mutableCopy];
    NSMutableArray *persons = nil;
    if (self.personsKeysSorted.count > indexPath.section) {
        NSString *key = self.personsKeysSorted[indexPath.section];
        if ([key isEqualToString:[person.firstName firstCharacter]]) {
            persons = dic[key];
        }
    }
    
    if (persons) {
        [persons insertObject:person atIndex:indexPath.row];
    } else {
        NSMutableArray *personsGroupe = [NSMutableArray arrayWithObject:person];
        [dic setObject:personsGroupe forKey:[person.firstName firstCharacter]];
    }
    
    self.indexPersons = [dic copy];
}

- (void)deletePersonWithIndexPath:(NSIndexPath *)indexPath {
    NSMutableDictionary *dic = [self.indexPersons mutableCopy];
    NSArray *keys = self.personsKeysSorted;
    if (keys.count > indexPath.section) {
        NSString *key = [keys[indexPath.section] mutableCopy];
        NSMutableArray *persons = [dic[key] mutableCopy];
        [persons removeObjectAtIndex:indexPath.row];
        [dic removeObjectForKey:key];
        persons.count == 0 ?: [dic setObject:persons forKey:key];
        self.indexPersons = [dic copy];
    }
}

- (void)movePersonWithIndexPath:(NSIndexPath *)indexPath {
    LUPerson *person = [self personWithIndexPath:indexPath];
    [self deletePersonWithIndexPath:indexPath];
    [self printPersons];
    [self addPerson:person];
}

- (LUPerson *)personWithIndexPath:(NSIndexPath *)indexPath {
    NSString *key = [self.personsKeysSorted objectAtIndex:indexPath.section];
    NSArray *persons = self.indexPersons[key];
    
    return persons[indexPath.row];
}

- (NSArray *)personsForSection:(NSInteger)section {
    NSArray *keys = self.personsKeysSorted;
    NSInteger lastSection = self.personsKeysSorted.count - 1;
    if (lastSection >= section ) {
        NSString *key = keys[section];
        
        return self.indexPersons[key];
    } else {
        NSLog(@"%@", kLUBeyondSections);
        
        return nil;
    }
}

- (NSMutableArray *)personsCount:(NSInteger)count withKey:(NSString *)key {
    NSMutableArray *persons = [NSMutableArray new];
    for (int i = 0; i < count; i++) {
        NSString *firstName = [NSString stringWithFormat:@"%@%iFN", key, i];
        NSString *lastName = [NSString stringWithFormat:@"%@%iLN", key, i];
        LUPerson *person = [LUPerson personWithFirstName:firstName lastName:lastName];
        [persons addObject:person];
    }
    
    return persons;
}

- (void)printPersons {
    NSLog(@"PERSON:->");
    NSArray *persons = [self.indexPersons allValues];
    for (NSString *firstName in [persons valueForKey:@"firstName"]) {
        NSLog(@"%@",firstName);
    }
}

#pragma mark Index Person

- (NSInteger)sectionForPerson:(LUPerson *)person {
    NSString *key = [person.firstName firstCharacter];
    NSArray *keys = self.personsKeysSorted;
    NSInteger section = [keys indexOfObject:key];
    if (section == NSNotFound) {
        section = [keys indexForString:key];
    }
    
    return section;
}

- (NSInteger)rowForPerson:(LUPerson *)person {
    NSString *key = [person.firstName firstCharacter];
    NSMutableArray *persons = self.indexPersons[key];
    
    return persons ? [persons indexForPerson:person] : 0;
}

- (NSIndexPath *)indexPathWithPerson:(LUPerson *)person {
    NSInteger section = [self sectionForPerson:person];
    NSInteger row = [self rowForPerson:person];
    
    return [NSIndexPath indexPathForRow:row inSection:section];
}

- (NSIndexPath *)updateNewIndexPath:(NSIndexPath *)newIndexPath
                       oldIndexPath:(NSIndexPath *)oldIndexPath {
    BOOL isNeedDeleteSection = [self countPersonsInSection:oldIndexPath.section] == 1;
    BOOL isNewPositionSectionMore = newIndexPath.section > oldIndexPath.section;
    
    if (isNeedDeleteSection && isNewPositionSectionMore) {
        return [NSIndexPath indexPathForRow:newIndexPath.row
                                  inSection:newIndexPath.section - 1];
    }
    
    return newIndexPath;
}

- (BOOL)isSectionWithPerson:(LUPerson *)person {
    return [self.personsKeysSorted containsObject:[person.firstName firstCharacter]];
}

- (NSInteger)countPersonsInSection:(NSInteger)section {
    NSArray *keys = self.personsKeysSorted;
    if (keys.count == 0) return 0;
    
    NSString *key = [keys objectAtIndex:section];
    NSArray *persons = self.indexPersons[key];
    
    return persons.count;
}

#pragma mark Modify TableView

- (void)deleteDataSectionRowWithIndexPath:(NSIndexPath *)oldIndexPath
                                andPerson:(LUPerson *)oldPerson
{
    BOOL isLastRow = [self countPersonsInSection:oldIndexPath.section] <= 1;
    BOOL isLastSection = self.personsKeysSorted.count <= 1;
    
    [self printPersons];
    [self deletePersonWithIndexPath:oldIndexPath];
    [self printPersons];
    
    if (isLastRow && !isLastSection) {
        [self deleteSectionsWithIndexPath:oldIndexPath];
    } else {
        [self deleteRowWithIndexPath:oldIndexPath];
    }
}

- (void)createDataSectionRowWithIndexPath:(NSIndexPath *)newIndexPath
                                andPerson:(LUPerson *)newPerson
{
    BOOL isSectionWithPerson = [self isSectionWithPerson:newPerson] ||
    self.indexPersons.allValues.count == 0;
    [self addPerson:newPerson];
    if (isSectionWithPerson) {
        [self createRowWithIndexPath:newIndexPath];
    } else {
        [self createSectionsWithIndexPath:newIndexPath];
    }
}

- (void)changeDataRowWithIndexPath:(NSIndexPath *)oldIndexPath
                      newIndexPath:(NSIndexPath *)newIndexPath
                         oldPerson:(LUPerson *)oldPerson
                         newPerson:(LUPerson *)newPerson
{
        [oldPerson overwriteFromPerson:newPerson];
        [self movePersonWithIndexPath:oldIndexPath];
        newIndexPath = [self indexPathWithPerson:newPerson];
        [self moveRowAtIndexPath:oldIndexPath toIndexPath:newIndexPath];
}

#pragma mark Section and Row

- (void)deleteSectionsWithIndexPath:(NSIndexPath *)indexPath {
    __weak typeof(self) weakSelf = self;
    [self.tableView perfomUpdates:^{
        __strong typeof(self) self = weakSelf;
        [self insertButtonWithIndexPath:indexPath];
        NSLog(@"DELETE SECTION = %@", @(indexPath.section));
        NSIndexSet *indexSet = [NSIndexSet indexSetWithIndex:indexPath.section];
        [self.tableView deleteSections:indexSet withRowAnimation:UITableViewRowAnimationAutomatic];
    }];
}

- (void)createSectionsWithIndexPath:(NSIndexPath *)indexPath {
    __weak typeof(self) weakSelf = self;
    [self.tableView perfomUpdates:^{
        __strong typeof(self) self = weakSelf;
        [self deleteButtonWithIndexPath:indexPath];
        NSIndexSet *indexSet = [NSIndexSet indexSetWithIndex:indexPath.section];
        [self printPersons];
        [self.tableView insertSections:indexSet
                      withRowAnimation:UITableViewRowAnimationAutomatic];
    }];
}

- (void)reloadLastSections {
    __weak typeof(self) weakSelf = self;
    [self.tableView perfomUpdates:^{
        __strong typeof(self) self = weakSelf;
        NSIndexSet *indexSet = [NSIndexSet indexSetWithIndex:self.personsKeysSorted.count];
        //[self.tableView reloadSectionIndexTitles];
        [self.tableView reloadSections:indexSet withRowAnimation:UITableViewRowAnimationAutomatic];
    }];
}

- (void)deleteButtonWithIndexPath:(NSIndexPath *)indexPath {
    NSInteger buttonSection = self.personsKeysSorted.count - 2;
    buttonSection = buttonSection >= 0 ? buttonSection : 0;
    if (indexPath.section > buttonSection) {
        NSInteger buttonRow = [self personsForSection:buttonSection].count;
        NSIndexPath *buttonIndexPath= [NSIndexPath indexPathForRow:buttonRow inSection:buttonSection];
        [self.tableView deleteRowsAtIndexPaths:@[buttonIndexPath]
                              withRowAnimation:UITableViewRowAnimationAutomatic];
    }
}

- (void)insertButtonWithIndexPath:(NSIndexPath *)indexPath {
    NSInteger currentButtonSection = self.personsKeysSorted.count - 1;
    currentButtonSection = currentButtonSection >= 0 ? currentButtonSection : 0;
    NSInteger currentButtonRow = [self countPersonsInSection:currentButtonSection];
    if (indexPath.section > currentButtonSection) {
        NSIndexPath *newButtonIndexPath = [NSIndexPath indexPathForRow:currentButtonRow
                                                             inSection:currentButtonSection];
        [self.tableView insertRowsAtIndexPaths:@[newButtonIndexPath]
                              withRowAnimation:UITableViewRowAnimationAutomatic];
    }
}

- (void)deleteRowWithIndexPath:(NSIndexPath *)indexPath {
    __weak typeof(self) weakSelf = self;
    [self.tableView perfomUpdates:^{
        __strong typeof(self) self = weakSelf;
        [self.tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
    }];
}

- (void)createRowWithIndexPath:(NSIndexPath *)indexPath {
    __weak typeof(self) weakSelf = self;
    [self.tableView perfomUpdates:^{
        __strong typeof(self) self = weakSelf;
        [self.tableView insertRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
    }];
}

- (void)moveRowAtIndexPath:(NSIndexPath *)indexPath toIndexPath:(NSIndexPath *)newIndexPath {
    __weak typeof(self) weakSelf = self;
    [self.tableView perfomUpdates:^{
        __strong typeof(self) self = weakSelf;
        [self.tableView moveRowAtIndexPath:indexPath toIndexPath:newIndexPath];
    }];
    [self.tableView reloadRowsAtIndexPaths:@[indexPath, newIndexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
}

#pragma mark - Navigation

- (void)showPersonViewControllerWithPerson:(LUPerson *)person {
    PersonViewController *personViewController = [[PersonViewController alloc] initWithNibName:NSStringFromClass([PersonViewController class]) bundle:nil];
    
    personViewController.person = person;
    personViewController.personBlock = ^(LUPerson *newPerson, LUPerson *oldPerson) {
        
        NSIndexPath *newIndexPath = newPerson ? [self indexPathWithPerson:newPerson] : nil;
        NSIndexPath *oldIndexPath = oldPerson ? [self indexPathWithPerson:oldPerson] : nil;
        NSLog(@"---OLDsection = %@, OLDrow = %@",
              @(oldIndexPath.section), @(oldIndexPath.row));
        NSLog(@"---NEWsection = %@, NEWrow = %@",
              @(newIndexPath.section), @(newIndexPath.row));
        
        if (oldPerson) { //If changed Person
            BOOL isNotChangeIndexPath = [oldIndexPath compare:newIndexPath] == NSOrderedSame;
            BOOL iNotChangeFirstName = [oldPerson.firstName isEqualToString:newPerson.firstName];
            if (isNotChangeIndexPath && iNotChangeFirstName) return;
            
            BOOL isNotChangeSection = oldIndexPath.section == newIndexPath.section;
            if (isNotChangeSection && !isNotChangeIndexPath) {//If changed Person Row
                [self printPersons];
                [self changeDataRowWithIndexPath:oldIndexPath
                                    newIndexPath:newIndexPath
                                       oldPerson:oldPerson
                                       newPerson:newPerson];
                [self printPersons];
            } else {                                       //If changed Person section
                [self deleteDataSectionRowWithIndexPath:oldIndexPath
                                              andPerson:oldPerson];
                newIndexPath = [self indexPathWithPerson:newPerson];
                [self createDataSectionRowWithIndexPath:newIndexPath
                                              andPerson:newPerson];
            }
            
        } else { //If created persons
            [self createDataSectionRowWithIndexPath:newIndexPath andPerson:newPerson];
        }
    };
    
    [self.navigationController pushViewController:personViewController animated:YES];
}

#pragma mark - UITableViewDataSource methods

- (BOOL)isButtonForIndexPath:(NSIndexPath *)indexPath {
    NSArray *keys = self.personsKeysSorted;
    NSMutableArray *persons = self.indexPersons[keys.lastObject];
    BOOL isLastSection = indexPath.section >= ((NSInteger)keys.count - 1);
    BOOL isLastRow = indexPath.row >= (persons.count);
    NSLog(@"Button key = %@ row = %@", keys.lastObject, @(indexPath.row));
    
    return isLastSection && isLastRow;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    NSInteger count = self.personsKeysSorted.count;
    NSInteger retInt = count == 0 ?: count;
    NSLog(@"numberOfSectionsInTableView %@", @(retInt));
    
    return count == 0 ?: count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    NSArray *keys = self.personsKeysSorted;
    
    if (keys.count == 0) return 1;
    NSString *key = keys[section];
    NSArray *persons = self.indexPersons[key];
    if (((NSInteger)keys.count - 1) == section) {
        NSLog(@"numberOfRowsInSection persons.count + 1 = %@, section = %i", @(persons.count + 1), section);
        
        return (persons.count + 1);
    } else {
        NSLog(@"numberOfRowsInSection persons.count = %@, section = %i", @(persons.count), section);
        
        return persons.count;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if ([self isButtonForIndexPath:indexPath]) {
        ButtonCell *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([ButtonCell class])];
        [cell setCellTitle:LUTitleAddPerson()];
        __weak typeof(self) weakSelf = self;
        cell.buttonHandler = ^{
            __strong typeof(self) self = weakSelf;
            [self showPersonViewControllerWithPerson:nil];
        };
        
        return cell;
    }
    
    PersonCell *personCell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([PersonCell class])];
    LUPerson *person = [self personWithIndexPath:indexPath];
    [personCell fillWithPerson:person];
    NSLog(@"cellForRowAtIndexPath section = %@, row = %@",
          @(indexPath.section), @(indexPath.row));
    
    return personCell;
}

- (nullable NSArray<NSString *> *)sectionIndexTitlesForTableView:(UITableView *)tableView {
    return self.personsKeysSorted;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    return self.personsKeysSorted.count ? self.personsKeysSorted[section] : @"";
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return self.personsKeysSorted.count ? kHeightForHeader : 0.f;
}

#pragma mark - UITableViewDelegate methods

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(nonnull NSIndexPath *)indexPath {
    [self showPersonViewControllerWithPerson:[self personWithIndexPath:indexPath]];
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

- (NSArray<UITableViewRowAction *> *)tableView:(UITableView *)tableView editActionsForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewRowAction *action =
    [UITableViewRowAction rowActionWithStyle:UITableViewRowActionStyleDestructive
                                       title:@"Delete"
                                     handler:^(UITableViewRowAction * _Nonnull action,
                                               NSIndexPath * _Nonnull indexPath) {
        [self removePersonAtIndexPath:indexPath];
    }];
    
    return @[action];
}

- (void)removePersonAtIndexPath:(NSIndexPath *)indexPath {
    LUPerson *deletePerson = [self personWithIndexPath:indexPath];
    [self deleteDataSectionRowWithIndexPath:indexPath andPerson:deletePerson];
    if (self.personsKeysSorted.count == 0) {
        [self reloadLastSections];
    }
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
        return ![self isButtonForIndexPath:indexPath];
}

@end
