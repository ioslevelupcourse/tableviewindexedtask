//
//  PersonViewController.m
//  TableViewIndexedTask
//
//  Created by Admin on 23.04.17.
//  Copyright © 2017 Nesynov. All rights reserved.
//

#import "PersonViewController.h"

#import "LUPerson.h"

#import "ImageViewCell.h"
#import "TextFieldCell.h"
#import "TextViewCell.h"
#import "ButtonCell.h"

#import "LUNibBasedView.h"
#import "ActionSheetPickerView.h"
#import "ActionSheetPickerData.h"

#import "UITableView+LUExtentions.h"
#import "NSString+LUExtentions.h"
#import "UIViewController+LUExtentions.h"
#import "NSDate+LUExtentions.h"

#import "LUConstants.h"

typedef NS_ENUM(NSInteger, LUIsOpen) {
    LUIsOpenNot = 0,
    LUIsOpenKeyboard,
    LUIsOpenPickerView,
    LUIsOpenPickerData,
};

typedef NS_ENUM(NSInteger, LUPersonProperty) {
    LUPersonImage = 0,
    LUPersonFirstName,
    LUPersonLastName,
    LUPersonBirthday,
    LUPersonCountry,
    LUPersonAbout,
    LUpersonSave,
    LUPersonPropertyCount
};

@interface PersonViewController () <UITableViewDataSource, UITabBarDelegate, LUKeyboardNotifications, UIImagePickerControllerDelegate, UINavigationControllerDelegate>

@property (nonatomic, strong) IBOutlet UITableView *tableView;

@property (nonatomic, strong) ActionSheetPickerView *pickerView;
@property (nonatomic, strong) ActionSheetPickerData *pickerData;

@property (nonatomic, strong) LUPerson *localPerson;
@property (nonatomic, assign, readonly) LUIsOpen isOpen;

@end

@implementation PersonViewController

#pragma mark - Life Cycle

- (void)viewDidLoad {
    [super viewDidLoad];
    
    UITableView *tableView = self.tableView;
    [tableView registerCellWithClass:[ImageViewCell class]];
    [tableView registerCellWithClass:[TextFieldCell class]];
    [tableView registerCellWithClass:[TextViewCell class]];
    [tableView registerCellWithClass:[ButtonCell class]];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [self hidePickersForce];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [self subscribeToKeyboardNotifications];
}

- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    [self unsubscribeFromKeyboardNotifications];
}

- (void)setPerson:(LUPerson *)person {
    _person = person;
    self.localPerson = person != nil ? [self.person copy] : [LUPerson new];
}

#pragma mark - Privat methods pickers

- (NSArray *)countryNames {
    NSMutableArray *accumulator = [NSMutableArray new];
    NSArray *arrayISOCountryCodes = [NSLocale ISOCountryCodes];
    for (id countryCode in arrayISOCountryCodes) {
        NSLocale *locale = [NSLocale currentLocale];
        NSString *item = [locale displayNameForKey:NSLocaleCountryCode
                                             value:countryCode];
        [accumulator addObject:item];
    }
    
    return [accumulator copy];
}

- (void)pickerViewShowWithItem:(UITextField *)textField {
    self.pickerView = [ActionSheetPickerView stringPickerWithItems:[self countryNames] startSelectItem:textField.text selectCallBack:^(id selected) {
        [self resultOnPickerSelected:selected inTextField:textField];
        self.pickerView = nil;
        [self hideIsOpen:LUIsOpenNot];
        NSLog(@"selected: %@", selected);
    }];
    [self.pickerView presentPicker];
}

- (void)pickerDataShowWithItem:(UITextField *)textField {
    NSDate *date = [textField.text dateFromString];
    self.pickerData =
    [ActionSheetPickerData pickerDataWithStartSelectItem:date
                                          selectCallBack:^(id selected) {
                                              [self resultOnPickerSelected:selected inTextField:textField];
                                              self.pickerData = nil;
                                              [self hideIsOpen:LUIsOpenNot];
                                              NSLog(@"selected: %@", selected);
                                          }];
    [self.pickerData presentPicker];
}

- (void)resultOnPickerSelected:(id)selected inTextField:(UITextField *)textField {
    if (!selected) {
        NSLog(@"Canceled");
        return;
    }
    
    if ([selected isKindOfClass:[NSDate class]]) {
        NSDate *date = (NSDate *)selected;
        textField.text = [date stringFromDate];
        self.localPerson.birthday = date;
    } else {
        NSString *text = [NSString stringWithFormat:@"%@", selected];
        textField.text = text;
        self.localPerson.country = text;
    }
}

#pragma mark - Privat method support

- (NSString *)stringFromLUIsOpen:(LUIsOpen)open {
    const char* strings[] = {EP(LUIsOpenNot),
        EP(LUIsOpenKeyboard),
        EP(LUIsOpenPickerView),
        EP(LUIsOpenPickerData)
    };
    
    return [NSString stringWithFormat:@"%s", strings[open]];
}

#pragma mark - Navigation

- (void)sendPerson {
    if (self.person) {
        self.personBlock(self.localPerson, self.person);
    } else {
        self.personBlock(self.localPerson, nil);
    }
}

#pragma mark - Validation

- (BOOL)isValidText:(NSString *)text {
    BOOL result = YES;
    NSString *message = nil;
//    if (!text.length) {
//        message = LUMSGEmptyInput();
//        result = NO;
//    } else if (text.length <= 2) {
//        message = LUMSGThanTwoCharacters();
//        result = NO;
//    } else if (text.length > 40) {
//        message = LUMSGNoMoreThanCharacters();
//        result = NO;
//    } else if (!text.isValidName) {
//        message = LUMSGPostAnIncorrectEntry();
//        result = NO;
//    }
    
    if (!result) {
        [self presentAlert:LUTitleDataInput()
                   message:message
                needCancel:NO
                 okHandler:nil];
    
        return result;
    }
    
    return YES;
}

- (BOOL)returnIsValidText:(NSString *)text {
    BOOL result = [self isValidText:text];
    if (result) {
        [self hideKeyboard];
    }
    
    return result;
}

- (BOOL)validLocalPerson {
    LUPerson *person = self.localPerson;
    
    return [self isValidText:person.firstName] && [self isValidText:person.lastName];
}

#pragma mark - Hide new View

- (void)hidePickersForce {
    if (self.pickerView) {
        [self.pickerView dismissPickerForce];
        self.pickerView = nil;
    } else if (self.pickerData) {
        [self.pickerData dismissPickerForce];
        self.pickerData = nil;
    }
}

- (void)hidePickerView {
    if (!self.pickerView) return;
    [self.pickerView dismissPicker];
    self.pickerView = nil;
}

- (void)hidePickerData {
    if (!self.pickerData) return;
    [self.pickerData dismissPicker];
    self.pickerData = nil;
}

- (void)hidePickers {
    [self hidePickerView];
    [self hidePickerData];
}

- (void)hideKeyboard {
    [self.view endEditing:YES];
}

- (void)hidePikersAndKeyboard {
    [self hideKeyboard];
    [self hidePickers];
}

- (void)hideIsOpen:(LUIsOpen)open {
    NSLog(@"SELF.isOpen = %@", [self stringFromLUIsOpen:self.isOpen]);
    NSLog(@"OPEN = %@", [self stringFromLUIsOpen:open]);
    if (self.isOpen == open) return;
    _isOpen = open;
    switch (open) {
        case LUIsOpenKeyboard: {
            [self hidePickers];
            break;
        }
        case LUIsOpenPickerView: {
            [self hideKeyboard];
            [self hidePickerData];
            break;
        }
        case LUIsOpenPickerData: {
            [self hideKeyboard];
            [self hidePickerView];
            break;
        }
        case LUIsOpenNot: {
            break;
        }
    }
}

- (void)takeImageClicked {
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
        UIAlertController *selectionController = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleActionSheet];
        [selectionController addAction:[UIAlertAction actionWithTitle:@"Take photo" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            [self showPickerForSource:UIImagePickerControllerSourceTypeCamera];
        }]];
        [selectionController addAction:[UIAlertAction actionWithTitle:@"Select from album" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            [self showPickerForSource:UIImagePickerControllerSourceTypePhotoLibrary];
        }]];
        [selectionController addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {}]];
        [self presentViewController:selectionController animated:YES completion:nil];
    } else {
        [self showPickerForSource:UIImagePickerControllerSourceTypePhotoLibrary];
    }
}

- (void)showPickerForSource:(UIImagePickerControllerSourceType)source {
    UIImagePickerController *imagePicker = [[UIImagePickerController alloc] init];
    imagePicker.sourceType = source;
    imagePicker.editing = YES;
    imagePicker.delegate = self;
    [self presentViewController:imagePicker animated:YES completion:nil];
}
#pragma mark - UIImagePickerControllerDelegate

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<NSString *,id> *)info {
    UIImage *image = info[UIImagePickerControllerOriginalImage];
    self.localPerson.image = image;
    [self.tableView reloadData];
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - Notification keyboard

- (CGFloat)takeHeightKeyboardOfUserInfo:(NSDictionary*)dictionary {
    CGRect keyboardFrame = [dictionary [UIKeyboardFrameEndUserInfoKey] CGRectValue];
    double height = keyboardFrame.size.height;
    
    return height;
}
- (void)keyboardWillShow:(NSNotification *)notification {
    [self hideIsOpen:LUIsOpenKeyboard];
}

- (void)keyboarWillHide:(NSNotification *)notification {
    [self hideIsOpen:LUIsOpenNot];
}

#pragma mark - UITableViewDataSource methods

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return LUPersonPropertyCount;
}

- (TextFieldCell *)createTextFieldCell {
    return [self.tableView dequeueReusableCellWithIdentifier:NSStringFromClass([TextFieldCell class])];
}

- (ImageViewCell *)imageCellWithTableView:(UITableView *)tableView {
    ImageViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:NSStringFromClass([ImageViewCell class])];
    [cell fillWithPerson:self.localPerson];
    cell.tapImageBlock = ^(ImageViewCell *cell) {
        [self takeImageClicked];
    };
    
    return cell;
}

- (TextFieldCell *)textFieldCellWithTableView:(UITableView *)tableView
                                         text:(NSString *)text
                                  placeholder:(NSString *)placeholder
                                updateHandler:(StringBlock)handler
{
    TextFieldCell *cell = [self createTextFieldCell];
    [cell setCellPlaceholder:placeholder];
    [cell setCellText:text];
    
    cell.shouldChangeCharactersInRangeBlock = ^BOOL(TextFieldCell *cell, NSString *newString) {
        BOOL isValid = [newString isValidName];
        if (isValid) {
            if (handler) {
                handler(newString);
            }
        }
        
        return isValid;
    };
    
    __weak typeof(self) weakSelf = self;
    cell.shouldReturnBlock = ^BOOL(TextFieldCell *cell) {
        __strong typeof(self) self = weakSelf;
        return [self returnIsValidText:TRIM([[cell getTextField] text])];
    };
    
    return cell;
}

- (TextFieldCell *)pickerInTextFieldCellWithTableView:(UITableView *)tableView
                                             selected:(id)selected
                                          placeholder:(NSString *)placeholder
                                                 open:(LUIsOpen)open
{
    TextFieldCell *cell = [self createTextFieldCell];
    UITextField *textField = [cell getTextField];
    [cell setCellPlaceholder:placeholder];
    [self resultOnPickerSelected:selected inTextField:textField];
    
    __weak typeof(self) weakSelf = self;
    cell.shouldBeginEditingBlock = ^BOOL(TextFieldCell *cell) {
        __strong typeof(self) self = weakSelf;
        if (self.isOpen == open) return NO;
        [self hideIsOpen:open];
        if (open == LUIsOpenPickerData) {
            [self pickerDataShowWithItem:textField];
        } else {
            [self pickerViewShowWithItem:textField];
        }
        
        return NO;
    };
    
    return cell;
}

- (TextViewCell *)textViewCellWithTableView:(UITableView *)tableView {
    TextViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:NSStringFromClass([TextViewCell class])];
    [cell setCellText:self.localPerson.about];
    
    cell.shouldChangeTextInRangeBlock = ^BOOL(TextViewCell *cell, NSString *newString) {
        self.localPerson.about = newString;
        return YES;
    };
    
    return cell;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    switch (indexPath.row) {
        case LUPersonImage: {
            return [self imageCellWithTableView:tableView];
        }
        case LUPersonFirstName: {
            return [self textFieldCellWithTableView:tableView
                                               text:self.localPerson.firstName
                                        placeholder:[self placeholderRow:indexPath.row]
                                      updateHandler:^(NSString *string) {
                                          self.localPerson.firstName = string;
                                      }];
        }
        case LUPersonLastName: {
            return [self textFieldCellWithTableView:tableView
                                               text:self.localPerson.lastName
                                        placeholder:[self placeholderRow:indexPath.row]
                                      updateHandler:^(NSString *string) {
                                          self.localPerson.lastName = string;
                                      }];
            
        }
        case LUPersonBirthday: {
            return [self pickerInTextFieldCellWithTableView:tableView
                                                   selected:self.localPerson.birthday
                                                placeholder:[self placeholderRow:indexPath.row]
                                                       open:LUIsOpenPickerData];
        }
        case LUPersonCountry: {
            return [self pickerInTextFieldCellWithTableView:tableView
                                                   selected:self.localPerson.country
                                                placeholder:[self placeholderRow:indexPath.row]
                                                       open:LUIsOpenPickerView];
        }
        case LUPersonAbout: {
            return [self textViewCellWithTableView:tableView];
        }
        case LUpersonSave: {
            ButtonCell *cell= [self.tableView dequeueReusableCellWithIdentifier:NSStringFromClass([ButtonCell class])];
            [cell setCellTitle:LUTitleSave()];
            cell.buttonHandler = ^{
                if ([self validLocalPerson]) {
                    [self sendPerson];
                    [self.navigationController popToRootViewControllerAnimated:YES];
                }
            };
            
            return cell;
        }
    }
    
    return [UITableViewCell new];
}

- (NSString *)placeholderRow:(NSInteger)row {
    switch (row) {
        case LUPersonFirstName: {
            return @"FirstName";
        }
        case LUPersonLastName: {
            return @"LastName";
        }
        case LUPersonBirthday: {
            return @"Birthday";
        }
        case LUPersonCountry: {
            return @"Country";
        }
        default:
            return @"";
    }
}

#pragma mark - UITableViewDelegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    switch (indexPath.row) {
        case LUPersonImage: {
            return 100.f;
        }
        case LUPersonAbout: {
            return UITableViewAutomaticDimension;
        }
        default:
            return 44.f;
    }
}

//- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath {
//    switch (indexPath.row) {
//        case LUPersonImage: {
//            return 200.f;
//        }
//        case LUPersonAbout: {
//            return 150.f;
//        }
//        default:
//            return 70.f;
//    }
//}

@end
