//
//  PersonViewController.h
//  TableViewIndexedTask
//
//  Created by Admin on 23.04.17.
//  Copyright © 2017 Nesynov. All rights reserved.
//

#import <UIKit/UIKit.h>

@class LUPerson;

@interface PersonViewController : UIViewController

#pragma mark - Public

@property (nonatomic, strong) LUPerson *person;
@property (nonatomic, copy) void (^personBlock)(LUPerson *newPerson, LUPerson *oldPerson);

@end
