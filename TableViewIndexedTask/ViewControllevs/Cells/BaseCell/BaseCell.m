//
//  BaseCell.m
//  TableViewIndexedTask
//
//  Created by Admin on 06.05.17.
//  Copyright © 2017 Nesynov. All rights reserved.
//

#import "BaseCell.h"

@implementation BaseCell

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    self.selectionStyle = UITableViewCellSelectionStyleNone;
}


@end
