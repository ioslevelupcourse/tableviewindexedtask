//
//  TextViewCell.m
//  TableViewIndexedTask
//
//  Created by Admin on 23.04.17.
//  Copyright © 2017 Nesynov. All rights reserved.
//

#import "TextViewCell.h"
#import "LUPerson.h"

@interface TextViewCell() <UITextViewDelegate>

@property (nonatomic, strong) IBOutlet UITextView *textView;

@end

@implementation TextViewCell

#pragma mark - Public methods

- (void)setCellText:(NSString *)text {
    self.textView.text = text;
}

#pragma mark - <UITextViewDelegate>

- (BOOL)textViewShouldBeginEditing:(UITextView *)textView {
    return (self.shouldBeginEditingBlock == nil) ? YES : self.shouldBeginEditingBlock(self);
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    NSString *newText = [textView.text stringByReplacingCharactersInRange:range withString:text];
    if ([text  isEqual: @"\n"]) {
        [textView resignFirstResponder];
        return NO;
    }
    
    if (newText.length == 0) return YES;
    
    return (self.shouldChangeTextInRangeBlock == nil) ? YES : self.shouldChangeTextInRangeBlock(self, newText);
}

@end
