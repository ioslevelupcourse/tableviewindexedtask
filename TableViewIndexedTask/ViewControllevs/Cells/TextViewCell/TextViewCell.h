//
//  TextViewCell.h
//  TableViewIndexedTask
//
//  Created by Admin on 23.04.17.
//  Copyright © 2017 Nesynov. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseCell.h"

@class LUPerson;

@interface TextViewCell : BaseCell

@property (nonatomic, copy) BOOL(^shouldBeginEditingBlock)(TextViewCell *cell);
@property (nonatomic, copy) BOOL(^shouldChangeTextInRangeBlock)(TextViewCell *cell, NSString *newString);

- (void)setCellText:(NSString *)text;

@end
