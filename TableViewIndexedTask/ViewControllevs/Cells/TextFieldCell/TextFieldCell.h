//
//  TextFieldCell.h
//  TableViewIndexedTask
//
//  Created by Admin on 23.04.17.
//  Copyright © 2017 Nesynov. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseCell.h"

typedef void(^CellBlock)(void);

@class LUPerson;

@interface TextFieldCell : BaseCell

@property (nonatomic, copy) BOOL(^shouldReturnBlock)(TextFieldCell *cell);
@property (nonatomic, copy) BOOL(^shouldBeginEditingBlock)(TextFieldCell *cell);
@property (nonatomic, copy) BOOL(^shouldChangeCharactersInRangeBlock)(TextFieldCell *cell, NSString *newString);

- (void)setCellPlaceholder:(NSString *)placeholder;
- (void)setCellText:(NSString *)text;

- (UITextField *)getTextField;

@end
