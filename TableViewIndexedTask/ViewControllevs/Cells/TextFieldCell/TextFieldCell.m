//
//  TextFieldCell.m
//  TableViewIndexedTask
//
//  Created by Admin on 23.04.17.
//  Copyright © 2017 Nesynov. All rights reserved.
//

#import "TextFieldCell.h"
#import "ActionSheetPickerView.h"
#import "ActionSheetPickerData.h"

#import "AppHelper.h"

@interface TextFieldCell () <UITextFieldDelegate>

@property (nonatomic, strong) IBOutlet UITextField *textField;

@property (nonatomic, strong) ActionSheetPickerView *pickerView;
@property (nonatomic, strong) ActionSheetPickerData *pickerData;

@end

@implementation TextFieldCell

#pragma mark - Public

- (void)setCellText:(NSString *)text {
    self.textField.text = text;
}

- (void)setCellPlaceholder:(NSString *)placeholder {
    self.textField.placeholder = placeholder;
}

- (UITextField *)getTextField {
    return self.textField;
}

#pragma mark - <UITextFieldDelegate>

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    return (self.shouldReturnBlock == nil) ? YES : self.shouldReturnBlock(self);
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    return (self.shouldBeginEditingBlock == nil) ? YES : self.shouldBeginEditingBlock(self);
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    NSString *newStr = [textField.text stringByReplacingCharactersInRange:range withString:string];
    
    if (newStr.length == 0) return YES;
    
    return (self.shouldChangeCharactersInRangeBlock == nil) ? YES : self.shouldChangeCharactersInRangeBlock(self, newStr);
}

@end
