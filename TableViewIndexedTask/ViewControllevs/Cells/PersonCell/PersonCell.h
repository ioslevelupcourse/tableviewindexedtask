//
//  PersonCell.h
//  TableViewIndexedTask
//
//  Created by Admin on 23.04.17.
//  Copyright © 2017 Nesynov. All rights reserved.
//

#import <UIKit/UIKit.h>

@class LUPerson;

@interface PersonCell : UITableViewCell

@property (nonatomic, strong) IBOutlet UIImageView *personImageView;
@property (nonatomic, strong) IBOutlet UILabel *personLabel;

- (void)fillWithPerson:(LUPerson *)person;

@end
