//
//  PersonCell.m
//  TableViewIndexedTask
//
//  Created by Admin on 23.04.17.
//  Copyright © 2017 Nesynov. All rights reserved.
//

#import "PersonCell.h"
#import "LUPerson.h"

@implementation PersonCell

- (void)fillWithPerson:(LUPerson *)person {
    self.personImageView.image = person.image;
    self.personLabel.text = person.firstName;
}

@end
