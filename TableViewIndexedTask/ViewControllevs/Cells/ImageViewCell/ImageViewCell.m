//
//  ImageViewCell.m
//  TableViewIndexedTask
//
//  Created by Admin on 23.04.17.
//  Copyright © 2017 Nesynov. All rights reserved.
//

#import "ImageViewCell.h"

@interface ImageViewCell ()
@property (nonatomic, strong) IBOutlet UIImageView *personImageView;
@end

@implementation ImageViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(didTap:)];
    [self addGestureRecognizer:singleTap];

}

- (void)didTap:(UITapGestureRecognizer *)sender {
    NSLog(@"%@", sender);
    if (self.tapImageBlock) {
        self.tapImageBlock(self);
    }
}

- (void)fillWithPerson:(LUPerson *)person {
    self.personImageView.image = person.image;
}

@end
