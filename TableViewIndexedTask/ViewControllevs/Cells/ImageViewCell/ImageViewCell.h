//
//  ImageViewCell.h
//  TableViewIndexedTask
//
//  Created by Admin on 23.04.17.
//  Copyright © 2017 Nesynov. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LUPerson.h"
#import "BaseCell.h"

@interface ImageViewCell : BaseCell

@property (nonatomic, copy) void(^tapImageBlock)(ImageViewCell *cell);

- (void)fillWithPerson:(LUPerson *)person;

@end
