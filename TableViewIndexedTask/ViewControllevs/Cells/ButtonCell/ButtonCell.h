//
//  ButtonCell.h
//  TableViewIndexedTask
//
//  Created by Admin on 23.04.17.
//  Copyright © 2017 Nesynov. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseCell.h"

typedef void(^CellBlock)(void);

@interface ButtonCell : BaseCell

@property (nonatomic, copy) CellBlock buttonHandler;

- (void)setCellTitle:(NSString *)cellTitle;

@end
