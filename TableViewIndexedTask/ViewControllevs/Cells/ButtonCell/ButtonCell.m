//
//  ButtonCell.m
//  TableViewIndexedTask
//
//  Created by Admin on 23.04.17.
//  Copyright © 2017 Nesynov. All rights reserved.
//

#import "ButtonCell.h"

@interface ButtonCell ()

@property (nonatomic, strong) IBOutlet UIButton *button;

@end

@implementation ButtonCell

- (void)setCellTitle:(NSString *)cellTitle {
    [self.button setTitle:cellTitle  forState:UIControlStateNormal];
}

#pragma mark - Action

- (IBAction)buttonClicked:(UIButton *)sender {
    NSLog(@"buttonClicked");
    if (self.buttonHandler) {
        self.buttonHandler();
    }
}


@end
