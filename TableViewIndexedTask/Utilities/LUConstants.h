//
//  LUConstants.h
//  TableViewIndexedTask
//
//  Created by Admin on 12.05.17.
//  Copyright © 2017 Nesynov. All rights reserved.
//

#pragma mark - Macroses

#define EP(x) [x] = #x
#define TRIM(string) [string trimWhitespace]

#pragma mark - Constant Public

static NSString *const LUDateFormat = @"dd-MMM-yyyy";

#pragma mark - Message

static inline NSString *LUMSGPostAnIncorrectEntry() {
    return NSLocalizedString(@"Post an incorrect entry", @"");
}

static inline NSString *LUMSGEmptyInput() {
    return NSLocalizedString(@"Empty input", @"");
}

static inline NSString *LUMSGThanTwoCharacters() {
    return NSLocalizedString(@"More than two characters", @"");
}

static inline NSString *LUMSGNoMoreThanCharacters() {
    return NSLocalizedString(@"No more than 40 characters", @"");
}

#pragma mark - Title

static inline NSString *LUTitleSave() {
    return NSLocalizedString(@"Save", @"");
}

static inline NSString *LUTitleAddPerson() {
    return NSLocalizedString(@"Add Person", @"");
}

static inline NSString *LUTitleDataInput() {
    return NSLocalizedString(@"Data Input", @"");
}





