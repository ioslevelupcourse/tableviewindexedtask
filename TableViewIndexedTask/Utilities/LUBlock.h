//
//  Block.h
//  TableViewIndexedTask
//
//  Created by Admin on 12.05.17.
//  Copyright © 2017 Nesynov. All rights reserved.
//

#import "LUPerson.h"


typedef void (^VoidBlock)(void);
typedef void (^StringBlock)(NSString *string);
