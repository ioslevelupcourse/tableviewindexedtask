//
//  LUPerson.h
//  TableViewTask
//
//  Created by Admin on 06.03.17.
//  Copyright © 2017 Nesynov. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSInteger, LUPersonSex) {
    LUPersonMale = 0,
    LUPersonFemale
};

@interface LUPerson : NSObject <NSCopying>
@property (nonatomic, copy) NSString *firstName;
@property (nonatomic, copy) NSString *lastName;
@property (nonatomic, copy) NSString *country;
@property (nonatomic, assign) LUPersonSex sex;
@property (nonatomic, strong) NSDate *birthday;
@property (nonatomic, strong) UIImage *image;
@property (nonatomic, copy) NSString *about;

+ (LUPerson *)personWithFirstName:(NSString *)firstName lastName:(NSString *)lastName;
- (instancetype)initWithFirstName:(NSString *)firstName lastName:(NSString *)lastName;

- (LUPerson *)overwriteFromPerson:(LUPerson *)person;
- (NSString *)allPropertyToString;

@end
