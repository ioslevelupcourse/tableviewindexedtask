//
//  LUPerson.m
//  TableViewTask
//
//  Created by Admin on 06.03.17.
//  Copyright © 2017 Nesynov. All rights reserved.
//

#import "LUPerson.h"
#import "AppHelper.h"

#import "NSDate+LUExtentions.h"
#import "LUConstants.h"

@implementation LUPerson

#pragma mark - Initialize

- (instancetype)initWithFirstName:(NSString *)firstName lastName:(NSString *)lastName {
    self = [super init];
    if (self) {
        _firstName = firstName;
        _lastName = lastName;
        _sex = YES;
        _country = @"Ukraine";
        _birthday = [NSDate date];
    }
    
    return self;
}

+ (LUPerson *)personWithFirstName:(NSString *)firstName lastName:(NSString *)lastName {
    return [[LUPerson alloc] initWithFirstName:firstName lastName:lastName];
}

#pragma mark - Public

- (NSString *)allPropertyToString {
    NSString* string = [NSString stringWithFormat:
                        @"%@ %@ %@ %@ %@",
                        self.firstName,
                        self.lastName,
                        [self stringFromLUPersonSex:self.sex],
                        self.country,
                        [self.birthday stringFromDate]];
    
    return string;
}

- (NSString *)stringFromLUPersonSex:(LUPersonSex)sex {
    const char* strings[] = {EP(LUPersonMale), EP(LUPersonFemale)};
    return [NSString stringWithFormat:@"%s", strings[sex]];
}

- (id)copyWithZone:(NSZone *)zone {
    LUPerson *copy = [[[self class] allocWithZone:zone] init];
    if (copy) {
        copy.firstName = self.firstName;
        copy.lastName = self.lastName;
        copy.country = self.country;
        copy.sex = self.sex;
        copy.birthday = self.birthday;
        copy.image = self.image;
        copy.about = self.about;
    }
    
    return copy;
}

- (LUPerson *)overwriteFromPerson:(LUPerson *)person {
    if (![self.firstName isEqualToString:person.firstName]) {
        self.firstName = person.firstName;
    }
    
    if (![self.lastName isEqualToString:person.lastName]) {
        self.lastName = person.lastName;
    }
    
    if (![self.country isEqualToString:person.country]) {
        self.country = person.country;
    }
    
    if (self.sex != person.sex) {
        self.sex = person.sex;
    }
    
    if (![self.birthday isEqualToDate:person.birthday]) {
        self.birthday = person.birthday;
    }

    self.image = person.image;
    
    if (![self.about isEqualToString:person.about]) {
        self.about = person.about;
    }
    
    return self;
}

@end
