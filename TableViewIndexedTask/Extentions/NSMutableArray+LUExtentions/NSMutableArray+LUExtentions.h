//
//  NSMutableArray+LUExtentions.h
//  TableViewIndexedTask
//
//  Created by Admin on 17.05.17.
//  Copyright © 2017 Nesynov. All rights reserved.
//

#import <UIKit/UIKit.h>

@class LUPerson;

@interface NSMutableArray (LUExtentions)

#pragma mark - Object methods

- (void)sortMutablePersons;
- (void)sortMutableStrings;

@end
