//
//  NSMutableArray+LUExtentions.m
//  TableViewIndexedTask
//
//  Created by Admin on 17.05.17.
//  Copyright © 2017 Nesynov. All rights reserved.
//

#import "NSMutableArray+LUExtentions.h"
#import "LUPerson.h"

@implementation NSMutableArray(LUExtentions)

- (void)sortMutablePersons {
    [self sortUsingComparator:^NSComparisonResult(LUPerson *person1, LUPerson *person2) {
        return [person1.firstName compare:person2.firstName];
    }];
}

- (void)sortMutableStrings {
    [self sortUsingComparator:^NSComparisonResult(NSString *str1, NSString *str2) {
        return [str1 compare:str2];
    }];
}

@end
