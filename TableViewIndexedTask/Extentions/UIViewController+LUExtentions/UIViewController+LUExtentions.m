//
//  UIViewController+LUExtentions.m
//  TableViewIndexedTask
//
//  Created by Admin on 17.05.17.
//  Copyright © 2017 Nesynov. All rights reserved.
//

#import "UIViewController+LUExtentions.h"

@implementation UIViewController (LUExtentions)

- (void)presentAlert:(NSString *)title
             message:(NSString *)message
          needCancel:(BOOL)needCancel
           okHandler:(VoidBlock)okHandler;
{
    
    UIAlertController *alertView = [UIAlertController alertControllerWithTitle: title
                                                                       message: message
                                                                preferredStyle: UIAlertControllerStyleAlert];
    
    if (needCancel) {
        [alertView addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:nil]];
    }
    
    [alertView addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        if (okHandler) {
            okHandler();
        }
    }]];
    
    [self presentViewController:alertView animated:true completion:nil];
}

- (void)subscribeToKeyboardNotifications {
    if (![self conformsToProtocol:@protocol(LUKeyboardNotifications)]) {
        NSLog(@"%@ do not protocol LUKeyboardNotifications", self);
        return;
    }
    
    NSNotificationCenter *notificationCenter = [NSNotificationCenter defaultCenter];
    [notificationCenter addObserver:self
                           selector:@selector(keyboardWillShow:)
                               name:UIKeyboardWillShowNotification
                             object:nil];
    /*[notificationCenter addObserver:object
     selector:@selector(keyboarWillHide:)
     name:UIKeyboardWillChangeFrameNotification
     object:nil];*/
    [notificationCenter addObserver:self
                           selector:@selector(keyboarWillHide:)
                               name:UIKeyboardWillHideNotification
                             object:nil];
}

- (void)unsubscribeFromKeyboardNotifications {
    NSNotificationCenter *notificationCenter = [NSNotificationCenter defaultCenter];
    [notificationCenter removeObserver:self
                                  name:UIKeyboardWillShowNotification
                                object:nil];
    /*[notificationCenter removeObserver:object
     name:UIKeyboardWillChangeFrameNotification
     object:nil];*/
    [notificationCenter removeObserver:self
                                  name:UIKeyboardWillHideNotification
                                object:nil];
}


@end


