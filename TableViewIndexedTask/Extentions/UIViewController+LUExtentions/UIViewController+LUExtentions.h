//
//  UIViewController+LUExtentions.h
//  TableViewIndexedTask
//
//  Created by Admin on 17.05.17.
//  Copyright © 2017 Nesynov. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LUBlock.h"

@protocol LUKeyboardNotifications <NSObject>
@required
- (void)keyboardWillShow:(NSNotification *)notification;
- (void)keyboarWillHide:(NSNotification *)notification;
@end

@interface UIViewController (LUExtentions)

- (void)presentAlert:(NSString *)title
             message:(NSString *)message
          needCancel:(BOOL)needCancel
           okHandler:(VoidBlock)okHandler;

- (void)subscribeToKeyboardNotifications;
- (void)unsubscribeFromKeyboardNotifications;

@end
