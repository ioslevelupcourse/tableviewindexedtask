//
//  NSDate+LUExtentions.m
//  TableViewIndexedTask
//
//  Created by Admin on 17.05.17.
//  Copyright © 2017 Nesynov. All rights reserved.
//

#import "NSDate+LUExtentions.h"

#import "Block.h"
#import "LUConstants.h"

@implementation NSDate(LUExtentions)

+ (NSDateFormatter *)formatter {
    NSDateFormatter *formatter = [NSDateFormatter new];
    formatter.dateFormat = LUDateFormat;
    
    return formatter;
}

- (NSString *)stringFromDate {
    return self ? [[NSDate formatter] stringFromDate:self] : nil;
}

@end
