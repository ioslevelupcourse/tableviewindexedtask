//
//  NSDate+LUExtentions.h
//  TableViewIndexedTask
//
//  Created by Admin on 17.05.17.
//  Copyright © 2017 Nesynov. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NSDate (LUExtentions)

#pragma mark - Class methods

+ (NSDateFormatter *)formatter;

#pragma mark - Object methods

- (NSString *)stringFromDate;

@end
