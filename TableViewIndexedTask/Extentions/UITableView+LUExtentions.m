//
//  UITableView+LUExtentions.m
//  TableViewIndexedTask
//
//  Created by Admin on 23.04.17.
//  Copyright © 2017 Nesynov. All rights reserved.
//

#import "UITableView+LUExtentions.h"

@implementation UITableView (LUExtentions)

- (void)perfomUpdates:(void(^)(void))handler {
    if (handler) {
        [self beginUpdates];
        handler();
        [self endUpdates];
    }
}

- (void)registerCellWithClass:(Class)classCell {
    NSString *identifier = NSStringFromClass(classCell);
    UINib *cellNib = [UINib nibWithNibName:identifier bundle:[NSBundle mainBundle]];
    [self registerNib:cellNib forCellReuseIdentifier:identifier];
}

@end
