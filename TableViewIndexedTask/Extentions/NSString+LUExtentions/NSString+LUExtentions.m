//
//  NSString+LUExtentions.m
//  TableViewIndexedTask
//
//  Created by Admin on 12.05.17.
//  Copyright © 2017 Nesynov. All rights reserved.
//

#import "NSString+LUExtentions.h"
#import "NSDate+LUExtentions.h"

@implementation NSString (LUExtentions)

- (BOOL)isValidName {
    if (!self.length) return NO;
    NSMutableCharacterSet *allowedSet = [NSMutableCharacterSet characterSetWithCharactersInString:@" "];
    [allowedSet formUnionWithCharacterSet:[NSCharacterSet letterCharacterSet]];
    NSCharacterSet *forbiddenSet = [allowedSet invertedSet];
    NSRange range = [self rangeOfCharacterFromSet:forbiddenSet];
    
//    return !(range.location != NSNotFound || self.length > 24);
    return YES;
}

- (BOOL)isValidAge {
    if (!self.length) return NO;
    NSCharacterSet *characters = [[NSCharacterSet decimalDigitCharacterSet] invertedSet];
    NSRange range = [self rangeOfCharacterFromSet:characters];
    if (range.location != NSNotFound || self.length > 3) {
        return NO;
    }
    
    NSInteger age = [self integerValue];
    
    return !(age < 0 || age > 110);
}


- (BOOL)stringIsEmpty {
    if (!self || self.length == 0) {
        return YES;
    } else {
        NSString *trimmedString = [self stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        
        return (trimmedString.length == 0);
    }
}

- (NSString *)trimWhitespace {
    return [self stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
}

- (NSString *)firstCharacter {
    return [self substringWithRange:NSMakeRange(0, 1)];
}

- (NSDate *)dateFromString {
    return self ? [[NSDate formatter] dateFromString:self] : nil;
}

@end
