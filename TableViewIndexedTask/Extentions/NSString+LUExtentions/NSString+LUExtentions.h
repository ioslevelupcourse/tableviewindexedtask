//
//  NSString+LUExtentions.h
//  TableViewIndexedTask
//
//  Created by Admin on 12.05.17.
//  Copyright © 2017 Nesynov. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NSString (LUExtentions)

- (BOOL)isValidName;
- (BOOL)isValidAge;
- (BOOL)stringIsEmpty;
- (NSString *)trimWhitespace;
- (NSString *)firstCharacter;
- (NSDate *)dateFromString;

@end
