//
//  UITableView+LUExtentions.h
//  TableViewIndexedTask
//
//  Created by Admin on 23.04.17.
//  Copyright © 2017 Nesynov. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UITableView (LUExtentions)

- (void)perfomUpdates:(void(^)(void))handler;
- (void)registerCellWithClass:(Class)classCell;

@end
