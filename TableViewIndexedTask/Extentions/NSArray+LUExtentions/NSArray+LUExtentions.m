//
//  NSArray+LUExtentions.m
//  TableViewIndexedTask
//
//  Created by Admin on 17.05.17.
//  Copyright © 2017 Nesynov. All rights reserved.
//

#import "NSArray+LUExtentions.h"
#import "LUPerson.h"

@implementation NSArray(LUExtentions)

- (NSArray *)sortPersons {
    return [self sortedArrayUsingComparator:^NSComparisonResult(LUPerson *person1, LUPerson *person2) {
        return [person1.firstName compare:person2.firstName];
    }];
}

- (NSArray *)sortString {
    return [self sortedArrayUsingComparator:^NSComparisonResult(NSString *str1, NSString *str2) {
        return [str1 compare:str2];
    }];
}

- (NSInteger)indexForString:(NSString *)string {
    return [self indexOfObject:string
                 inSortedRange:NSMakeRange(0, [self count])
                       options:NSBinarySearchingInsertionIndex
               usingComparator:^NSComparisonResult(NSString *str1, NSString *str2) {
                   return [str1 compare:str2];
               }];
}

- (NSInteger)indexForPerson:(LUPerson *)person {
    return [self indexOfObject:person
                 inSortedRange:NSMakeRange(0, [self count])
                       options:NSBinarySearchingInsertionIndex
               usingComparator:^NSComparisonResult(LUPerson *person1, LUPerson *person2) {
                   return [person1.firstName compare:person2.firstName];
               }];
}

@end
