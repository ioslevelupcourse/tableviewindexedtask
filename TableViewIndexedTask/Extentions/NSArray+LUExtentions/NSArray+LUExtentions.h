//
//  NSArray+LUExtentions.h
//  TableViewIndexedTask
//
//  Created by Admin on 17.05.17.
//  Copyright © 2017 Nesynov. All rights reserved.
//

#import <UIKit/UIKit.h>

@class LUPerson;

@interface NSArray (LUExtentions)

#pragma mark - Object methods

- (NSArray *)sortPersons;
- (NSArray *)sortString;

- (NSInteger)indexForPerson:(LUPerson *)person;
- (NSInteger)indexForString:(NSString *)string;

@end
